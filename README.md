## О Новостях дня ##

*Генератор предложений на русском языке*.
Написан на html/javascript, сервер - на python (google appengine).

Список слов можно скачать (в формате json) [во вкладке Downloads](http://bit.ly/1WLeEIP).
Или [вот тут](http://rudailynews.appspot.com/getjson). Тут самый новый

22.08.2015 - сайт перемещен на бесплатный хостинг aerobatic
22.02.2016 - сайт перемещен на сервер spiralarms 

[Сайт проекта](http://bit.ly/ruDN)

[Твиттер @rudailynews](http://bit.ly/1NynaXD)

##Андроид-версия##
[вот тут](http://bitbucket.org/Sheogorath2/rudailynews-android)

## Донатим ##

Вы можете сделать донат на [странице доната](http://spiralarms.org/donate.html)