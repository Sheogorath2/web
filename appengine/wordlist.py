# -*- coding: utf-8 -*-


# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import json,random,results,copy

__author__ = 'herman'

class WordsJSONList:
    def __init__(self, jsontext):
        self.json = jsontext
        self.wordlist = json.loads(self.json)

    def getwords(self,group):
        return(self.wordlist[group])

    def randomword(self,group):
        return random.choice(self.getwords(group))

    def addword(self,group,word):
        self.getwords(group).append(word)
        self.json = self.JSONize()

    def delword(self,group,word):
        b=self.getwords(group)
        a=copy.deepcopy(b)
        a[:] = [x for x in a if x.encode('utf-8') != word]
        if (a==b):
            return results.Results.FAILED
        self.wordlist[group]=a
        self.json = self.JSONize()
        print(self.JSONize(ascii=True))
        return results.Results.OK

    def count(self):
        return len(self.wordlist)

    def JSONize(self,ascii=True):
        return json.dumps(self.wordlist,ensure_ascii=ascii)

    def randomphrase(self):
        return self.randomword("subject")+" "+self.randomword("predicate")+" "+self.randomword("word3")

if __name__ == "__main__": #testing
    wjl=WordsJSONList("""{
    "subject": [
        "Свинья",
        "Собака",
        "Кошка"],
        "word3": [
        "в грязи",
        "у крыльца",
        "под ногами"],
        "predicate": [
        "хрюкает",
        "понимает",
        "мяукает"]
        }
        """)
    print(wjl.randomphrase())
    print
    wjl.addword("subject",u"кошечка кошечка")
    wjl.delword("subject","Свинья")
    for i in range(0,20):
        print(wjl.randomphrase())