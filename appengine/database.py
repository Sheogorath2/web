# -*- coding: utf-8 -*-
#!/usr/bin/env python


# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = 'herman'

"""Basic operations with NDB database"""

from google.appengine.ext import ndb
import wordlist
from results import Results

class JSONModel(ndb.Model):
    """Model of database entry that represents JSON word list"""
    json = ndb.TextProperty()

class KeyModel(ndb.Model):
    """Model of database entry that represents a key"""
    value = ndb.StringProperty()

def getjsonquery():
    """Get query of json word lists, if there is only one instance of them"""
    jsonquery = JSONModel.query()
    c = jsonquery.count()
    assert (c == 1), (("Более чем одна запись" if (c > 1) else "Нет записей") + " в базе данных!\n Всего записей: " + str(c))
    return jsonquery.fetch(1)

def getsentence():
    """Get randomly generated phrase. Returns tuple of two args."""
    try:
        result=getjsonquery()
    except AssertionError:
        return (Results.NORECORD,"")
    wlist=wordlist.WordsJSONList(result[0].json)
    return (Results.OK,wlist.randomphrase())

def addrecord(key):
    """Add an empty record to the database"""
    keyquery=KeyModel.query()

    isKey=checkkey(key)
    if (isKey==Results.WRONGKEY):
        return isKey

    if (JSONModel.query().count()>0):
        return Results.FAILED

    json = JSONModel(json="""{"subject":["БАЗА"],"predicate":["ДАННЫХ"],"word3":["ПУСТА!"]}""")
    json.put()

    return Results.OK

def changekey(key,newkey):
    """Change current key to new key"""
    keyquery=KeyModel.query()
    c=keyquery.count()
    if(c==0):
        thekey=KeyModel(value=newkey)
        thekey.put()
        return Results.OK
    elif(key==keyquery.fetch(1)[0].value):
        keyquery.fetch(1)[0].value=newkey
        keymodel=keyquery.fetch(1)[0].key.get()
        keymodel.value=newkey
        keymodel.put()
        return Results.OK
    return Results.WRONGKEY

def checkkey(key):
    """Check if key matches the database key"""
    keyquery=KeyModel.query()
    try:
        if(key!=keyquery.fetch(1)[0].value):
            raise IndexError
    except IndexError:
        return Results.WRONGKEY
    return Results.OK

def getrecord(jsonquery):
    try:
        json=jsonquery[0].key.get().json
        print(json.encode('utf-8'))
    except AssertionError:
        return Results.NORECORD
    return wordlist.WordsJSONList(json)

def addword(key,word,group):
    """Add word to group"""
    jsonquery=getjsonquery()
    isKey=checkkey(key)
    if (isKey==Results.WRONGKEY):
        return isKey
    record=getrecord(jsonquery)
    if (record==Results.NORECORD):
        return record
    wlist=record
    try:
        wlist.addword(group.encode('utf-8'),word.encode('utf-8'))
    except:
        return Results.NOGROUP
    jsonquery[0].key.delete()
    model = JSONModel(json=wlist.json)
    model.put()
    return Results.OK

def delword(key,word,group):
    """Delete all instances of word from the group"""
    jsonquery=getjsonquery()
    isKey=checkkey(key)
    if (isKey==Results.WRONGKEY):
        return isKey
    record=getrecord(jsonquery)
    if (record==Results.NORECORD):
        return record
    wlist=record
    try:
        result=wlist.delword(group.encode('utf-8'),word.encode('utf-8'))
    except KeyError:
        return Results.NOGROUP
    if (result==Results.OK):
        jsonquery[0].key.delete()
        model = JSONModel(json=wlist.json)
        model.put()
    return result

def getjson():
    """Get full word list JSON representation"""
    try:
        result=getjsonquery()
    except AssertionError:
        return (Results.NORECORD,"")
    wlist=wordlist.WordsJSONList(result[0].json)
    print(wlist.JSONize(ascii=False).encode('utf-8'))
    return wlist.JSONize(ascii=False)